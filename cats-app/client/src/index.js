import React from "react";
import { render } from "react-dom";
import { BrowserRouter } from "react-router-dom"
import { Provider } from "react-redux";

import App from "./app/App";
import store from "./app/store";
import "./style.scss"

const element = document.getElementById("root")

render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>,
  element
)

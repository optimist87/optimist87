// @flow

import React from "react";
import { connect } from "react-redux";

type PropsFromState = {
  isOnline: boolean,
}

type Props = PropsFromState & {
  onClick?: () => void,
  children: string,
  className?: string,
  type?: string,
  disabled?: boolean,
}

export function Button ({
  onClick,
  children,
  className,
  type,
  disabled,
  isOnline,
}: Props) {

  return (
    <button
      className={className || "btn"}
      type={type || "button"}
      onClick={onClick}
      disabled={disabled || !isOnline}
    >
      {children}
    </button>
  )
}

const mapStateToProps = (state: PropsFromState) => ({
  isOnline: state.isOnline,
})

export default connect<Props, *, *, *, *, *>(mapStateToProps)(Button)

import React from "react"
import { shallow } from "enzyme"
import { Header } from "../Header"

describe("<Header />", () => {
  it("snapshot", () => {
    const component = shallow(<Header />)
    expect(component).toMatchSnapshot()
  })

  test("Fav Link -> присутствуют 3 ссылки", () => {
    const props = {
      isAuthUser: true,
      favoritesList: [{"breeds":[],"id":"4he","url":"https://cdn2.thecatapi.com/images/4he.gif","width":400,"height":304}],
    }
    const component = shallow(<Header {...props} />)
    expect(component.find("nav > ul > li").length).toBe(3)
  })
})

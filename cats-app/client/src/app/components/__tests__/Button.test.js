import React from "react"
import { shallow } from "enzyme"
import { Button } from "../Button"

describe("<Button />", () => {
  it("snapshot", () => {
    const component = shallow(<Button />)
    expect(component).toMatchSnapshot()
  })

  it("snapshot with full props", () => {
    const props = {
      children: "Кнопка",
      className: "button",
      type: "submit",
      disabled: true,
    }
    const component = shallow(<Button {...props} />)
    expect(component).toMatchSnapshot()
  })
})

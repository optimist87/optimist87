// @flow

import React from "react";
import { connect } from "react-redux";

type Props = {
  isOnline: boolean,
}

export function Loader ({ isOnline }: Props) {

  return (
    <div className="loading">{isOnline ? "Загрузка..." : "Вы оффлайн..."}</div>
  )
}

const mapStateToProps = (state: Props) => ({
  isOnline: state.isOnline,
})

export default connect<Props, *, *, *, *, *>(mapStateToProps)(Loader)
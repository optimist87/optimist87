// @flow

import React, { useContext } from "react";
import { Formik } from "formik";
import Button from "~/app/components/Button"
import { REG_EXP_FOR_EMAIL } from "~/app/constants";
import api from "~/app/api";
import { AuthContext } from "~/app/context/AuthContext"

import type { FormikProps } from "formik";

export function LoginForm () {

  const auth = useContext(AuthContext)


  const initialValues = { email: "", password: "" }

  const handleSubmit = async (values, { setSubmitting, setErrors }) => {

      api.login(values)
        .then(response => {
          if (response.success) {
            setSubmitting(true)
            alert(response.message)
            auth.login(response.token, response.userId)
            window.location.replace("/")
          } else throw new Error(response.message)
        })
        .catch(errors => {
          setSubmitting(false)
          alert(errors.message)
          setErrors(errors.message)
        })

  }

  const validate = values => {
    let errors = {}
    if (!values.email) {
      errors.email = "Заполните email"
    } else if (
      REG_EXP_FOR_EMAIL.test(values.email)
    ) {
      errors.email = "Неправильный email"
    }
    if (!values.password) {
      errors.password = "Пустой пароль"
    }
    return errors
  }

  return (
    <Formik
      initialValues={initialValues}
      validate={validate}
      onSubmit={handleSubmit}
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
      }: FormikProps) => (
        <form className="form" onSubmit={handleSubmit}>
          <div className="field">
            <input
              type="email"
              name="email"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.email}
            />
            <div className="error">
              {errors.email && touched.email && errors.email}
            </div>
          </div>
          <div className="field">
            <input
              type="password"
              name="password"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.password}
            />
            <div className="error">
              {errors.password && touched.password && errors.password}
            </div>
          </div>
          <Button
            type="submit"
            disabled={isSubmitting}
          >
            Войти
          </Button>
        </form>
      )}
    </Formik>
  )

}

export default LoginForm

// @flow

import React, { useContext } from "react";
import { useHistory } from "react-router-dom"
import { AuthContext } from "~/app/context/AuthContext"
import Button from "~/app/components/Button"

type Props = {
  clearFavorites: () => void,
}

function SignOutButton ({ clearFavorites }: Props) {

  const auth = useContext(AuthContext)
  const history = useHistory()

  const signOut = () => {
    auth.logout()
    alert("Вы вышли")
    clearFavorites()
    history.push("/")
  }

  return (
    <Button
      className="link"
      onClick={signOut}
    >
      Выход
    </Button>
  )
}

export default SignOutButton

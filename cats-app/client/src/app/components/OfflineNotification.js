// @flow

import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import classNames from "classnames";

type Props = {
  isOnline: boolean,
}

export function OfflineNotification ({ isOnline }: Props) {

  const [show, setShow] = useState(false)

  useEffect(() => {
    setTimeout(() => {
      setShow(!isOnline)
    }, 100);
  }, [isOnline])

  if (isOnline) return null
  return <div className={classNames("notification", show && "notification--show")}>Вы Оффлайн...</div>
}

const mapStateToProps = (state: Props) => ({
  isOnline: state.isOnline,
})

export default connect<Props, *, *, *, *, *>(mapStateToProps)(OfflineNotification)

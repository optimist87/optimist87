// @flow

import React, { Fragment, useContext } from "react";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { clearFavorites } from "~/app/actions"
import { AuthContext } from "~/app/context/AuthContext"
import SignOutButton from "./auth/SignOut";

import type { CatItem } from "~/app/types";

type PropsFromState = {
  favoritesList: CatItem[],
  isAuthUser: boolean,
}

type Props = PropsFromState & {
  clearFavorites: () => void,
}

export function Header ({
  isAuthUser,
  favoritesList,
  clearFavorites
}: Props) {

  const isShowFavorites = isAuthUser && !!favoritesList.length
  return (
    <header className="header">
      <div className="header__inner">
        <nav>
          <ul>
            <li><NavLink to="/">Главная</NavLink></li>
            <li><NavLink to="/image">Котики</NavLink></li>
            {isShowFavorites && (<li><NavLink to="/favorites">Избранное</NavLink></li>)}
          </ul>
        </nav>
        <div className="auth">
          <ul>
            {isAuthUser ?
              <SignOutButton
                clearFavorites={clearFavorites}
              /> : (
              <Fragment>
                <li><NavLink to="/login">Вход</NavLink></li>
                <li><NavLink to="/register">Регистрация</NavLink></li>
              </Fragment>
            )}
          </ul>
        </div>
      </div>
    </header>
  )

}

const mapStateToProps = (state: PropsFromState) => ({
  favoritesList: state.favoritesList,
  isAuthUser: state.isAuthUser,
})

const mapDispatchToProps = dispatch => ({
  clearFavorites: () => dispatch(clearFavorites())
})

export default connect<Props, *, *, *, *, *>(
  mapStateToProps,
  mapDispatchToProps
)(Header)

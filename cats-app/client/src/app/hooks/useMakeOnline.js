// @flow

import { useState, useEffect } from "react";

export function useMakeOnline () {

  const [online, setOnline] = useState(true)

  useEffect(() => {
    makeOnline()
    window.addEventListener("online", makeOnline)
    window.addEventListener("offline", makeOnline)
    return () => {
      window.removeEventListener("online", makeOnline) // Удаление листнера
      window.removeEventListener("offline", makeOnline) // Удаление листнера
    }
  }, [])

  const makeOnline = () => setOnline(navigator.onLine)

  return online
}

export default useMakeOnline
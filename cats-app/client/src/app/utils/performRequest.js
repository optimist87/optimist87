// @flow

import type { PropsForRequest } from "~/app/types"

const getAuthToken = () => {
  const userDataString = localStorage.getItem("userData")
  if (!userDataString) return ""
  return JSON.parse(userDataString).token
}

export function performRequest ({
  url,
  method,
  body,
  headers = {},
}: PropsForRequest) {
  const request = async () => {
    try {
      if (body) {
        body = JSON.stringify(body)
      }
      headers["Content-Type"] = "application/json"
      if (!url.includes("http")) {
        headers["Authorization"] = getAuthToken()
      }
      const response = await fetch(url, { method: method, body: body, headers: headers })
      const data = await response.json()
      if (!response.ok) {
        throw new Error(data.message || "Что-то пошло не так")
      }
      return data
    } catch (e) {
      return new Error(e.message)
    }
  }

  return request()
}

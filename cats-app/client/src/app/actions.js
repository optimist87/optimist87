// @flow

import type { CatItem } from "~/app/types";

export const UPDATE_FAV = "UPDATE_FAV"
export const CLEAR_FAV = "CLEAR_FAV"
export const UPDATE_STATUS = "UPDATE_STATUS"
export const UPDATE_ONLINE = "UPDATE_ONLINE"

export const updateFavorites = (favoritesList: CatItem[]) => ({
  type: UPDATE_FAV,
  payload: favoritesList,
})

export const clearFavorites = () => ({
  type: CLEAR_FAV,
  payload: [],
})

export const updateAuthStatus = (status: boolean) => ({
  type: UPDATE_STATUS,
  payload: status
})

export const updateOnlineStatus = (online: boolean) => ({
  type: UPDATE_ONLINE,
  payload: online
})

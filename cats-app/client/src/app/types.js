// @flow

export type CatItem = {
  _id?: string,
  breeds?: Array<any>,
  categories?: Array<any>,
  height: number,
  id: string,
  url: string,
  width: number,
  owner?: string,
}

export type FavoritesAction = {
  type: string,
  payload: CatItem[],
}

export type StatusAction = {
  type: string,
  payload: boolean,
}

export type PropsForRequest = {
  url: string,
  method: string,
  body?: ?Object,
  headers?: Object,
}

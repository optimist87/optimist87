
import React from "react"
import { shallow } from "enzyme"
import { Cats } from "../Cats"

describe("<Cats />", () => {

  it("snap", () => {
    const props = {
      isAuthUser: true,
      favoritesList: [{"breeds":[],"id":"4he","url":"https://cdn2.thecatapi.com/images/4he.gif","width":400,"height":304}],
    }
    const component = shallow(<Cats {...props} />)
    expect(component).toMatchSnapshot()
  });

  // test("img src", () => {
  //   const component = shallow(<Cats />)
  //   // component.setState({ cats: [{"breeds":[],"id":"4he","url":"https://cdn2.thecatapi.com/images/4he.gif","width":400,"height":304}] });
  //   expect(component.find("img").props().src).toBe("https://cdn2.thecatapi.com/images/4he.gif")
  // });

  // test("addToFavorites", () => {
  //   const addToFavorites = jest.fn()
  //   const props = {
  //     isAuthUser: true,
  //     addToFavorites,
  //   }
  //   const component = shallow(<Cats {...props} />)
  //   component.setState({ cats: [{"breeds":[],"id":"4he","url":"https://cdn2.thecatapi.com/images/4he.gif","width":400,"height":304}], loading: false });
  //   component.find(".btn.add-to-fav").simulate("click");
  //   expect(props.addToFavorites).toHaveBeenCalledTimes(1)
  // });

  // test("deleteFromFavorites", () => {
  //   const deleteFromFavorites = jest.fn()
  //   const props = {
  //     isAuthUser: true,
  //     deleteFromFavorites,
  //     favoritesList: [{"breeds":[],"id":"4he","url":"https://cdn2.thecatapi.com/images/4he.gif","width":400,"height":304}],
  //   }
  //   const component = shallow(<Cats {...props} />)
  //   component.setState({ cats: [{"breeds":[],"id":"4he","url":"https://cdn2.thecatapi.com/images/4he.gif","width":400,"height":304}], loading: false });
  //   component.find(".btn.add-to-fav").simulate("click");
  //   expect(props.deleteFromFavorites).toHaveBeenCalledTimes(1)
  // });
})

// @flow

import React, { Fragment, useState, useEffect, useRef } from "react";
import { connect } from "react-redux";
import Loader from "~/app/components/Loader"
import Button from "~/app/components/Button"
import api from "~/app/api"

import type { CatItem } from "~/app/types";

type PropsFromState = {
  favoritesList: CatItem[],
  isAuthUser: boolean,
  isOnline: boolean,
}

type Props = PropsFromState & {
  addToFavorites: (CatItem) => void,
  deleteFromFavorites: (Array<string>) => void,
}

export function Cats ({
  isAuthUser,
  isOnline,
  addToFavorites,
  deleteFromFavorites,
  favoritesList
}: Props) {

  const [catItem, setCatItem] = useState([])
  const [allBreeds, setAllBreeds] = useState([])
  const [loading, setLoading] = useState(false)
  const [selectedBreed, setSelectedBreed] = useState("")

  const mounted = useRef(false)

  useEffect(() => {
    {
      mounted.current = true
      if (isOnline) {
        getBreeds()
        getCats(selectedBreed)
      }
    }
    return () => {
      mounted.current = false
    }
  }, [isOnline])

  const getCats = (breed?: string) => {
    setLoading(true)
    api.getCats(breed)
      .then(response => {
        if (mounted.current) {
          setCatItem(response)
          setLoading(false)
        }
      })
  }

  const getBreeds = () => {
    setLoading(true)
    api.getBreeds()
      .then(response => {
        if (mounted.current) {
          setAllBreeds(response)
          getInitialBreed()
          setLoading(false)
        }
      })
  }

  const getInitialBreed = () => {
    if (selectedBreed || !allBreeds.length) return
    const breed = allBreeds[0].id
    setSelectedBreed(breed)
  }

  const selectBreed = (event: any) => {
    const select: HTMLSelectElement = event.target
    setSelectedBreed(select.value);
    getCats(select.value)
  }

  return (
    <section>
      <h1>Котики</h1>
      <div className="cats__select">
        <select
          value={selectedBreed}
          onChange={selectBreed}
          disabled={!isOnline}
        >
          {allBreeds && allBreeds.map(breed => {
            return (<option key={breed.id} value={breed.id}>{breed.name}</option>)
          })}
        </select>
      </div>
      {loading ? (
        <Loader />
      ) : (
        <Fragment>
          <div className="cats__container">
            <div className="cats__img">
              {catItem && catItem.map(cat => {
                const isFavoriteImg = favoritesList.length && favoritesList.some(item => {return item.id === cat.id})
                return (
                  <div className="cats__img__inner" key={cat.id}>
                    <img src={cat.url} alt="" />
                    {isAuthUser &&
                      (!isFavoriteImg ?
                        (<Button
                          onClick={() => addToFavorites(cat)}
                        >
                          В избранное
                        </Button>) :
                        (<Button
                          onClick={() => deleteFromFavorites([cat.id])}
                        >
                          Удалить из избранного
                        </Button>)
                      )
                    }
                  </div>
                )
              })}
            </div>
          </div>
          <div className="cats__buttons">
            <Button
              onClick={() => getCats(selectedBreed)}
            >
              Еще котиков
            </Button>
          </div>
        </Fragment>
      )}
    </section>
  )
}

const mapStateToProps = (state: PropsFromState) => ({
  favoritesList: state.favoritesList,
  isAuthUser: state.isAuthUser,
  isOnline: state.isOnline,
})

export default connect<Props, *, *, *, *, *>(mapStateToProps)(Cats)

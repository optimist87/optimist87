// @flow

import React, { Fragment } from "react";

import RegisterForm from "~/app/components/auth/RegisterForm"

export function RegisterPage () {

  return (
    <Fragment>
      <h1>Регистрация</h1>
      <RegisterForm />
    </Fragment>
  )

}

export default RegisterPage


import React from "react"
import { shallow } from "enzyme"
import { FavoritesContainer } from "../FavoritesContainer"

describe("<FavoritesContainer />", () => {
  const addToFavorites = jest.fn()
  const deleteFromFavorites = jest.fn()
  const props = {
    favoritesList: [{"breeds":[],"id":"4he","url":"https://cdn2.thecatapi.com/images/4he.gif","width":400,"height":304}],
    addToFavorites,
    deleteFromFavorites
  }
  it("snapshot", () => {
    const component = shallow(<FavoritesContainer {...props} />)
    expect(component).toMatchSnapshot()
  });

  test("img src", () => {
    const component = shallow(<FavoritesContainer {...props} />)
    expect(component.find("img").props().src).toBe("https://cdn2.thecatapi.com/images/4he.gif")
  });

  // test("deleteList is not empty", () => {
  //   const deletedList = [{"breeds":[],"id":"4he","url":"https://cdn2.thecatapi.com/images/4he.gif","width":400,"height":304}]
  //   const component = shallow(<FavoritesContainer {...props} deletedList={deletedList} />)
  //   expect(component.find(".fav__container").length).toBe(2);
  // });

  test("deleteFromFavorites", () => {
    const component = shallow(<FavoritesContainer {...props} />)
    component.find("Button").simulate("click");
    expect(props.deleteFromFavorites).toHaveBeenCalledTimes(1)
  });
})

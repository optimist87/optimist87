
import React from "react"
import { shallow } from "enzyme"
import { Favorites } from "../Favorites"

describe("<Favorites />", () => {
  const addToFavorites = jest.fn()
  const deleteFromFavorites = jest.fn()
  const props = {
    online: true,
    isAuthUser: true,
    favoritesList: [{"breeds":[],"id":"4he","url":"https://cdn2.thecatapi.com/images/4he.gif","width":400,"height":304}],
    addToFavorites,
    deleteFromFavorites
  }

  it("snapshot", () => {
    const component = shallow(<Favorites {...props} />)
    expect(component).toMatchSnapshot()
  });

})

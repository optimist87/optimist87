// @flow

import React, { useState, useEffect, useContext } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom"
import classNames from "classnames";
import { AuthContext } from "~/app/context/AuthContext"
import Button from "~/app/components/Button"

import type { CatItem } from "~/app/types";

type PropsFromState = {
  favoritesList: CatItem[],
  isAuthUser: boolean,
}

type Props = PropsFromState & {
  addToFavorites: (CatItem) => void,
  deleteFromFavorites: (Array<string>) => void,
}

export function Favorites ({
  favoritesList,
  addToFavorites,
  deleteFromFavorites,
}: Props) {

  const [marked, setMarked] = useState([])

  useEffect(() => {
    console.log(marked)
    return () => {
      console.log(marked)
      // deleteFromFavorites(marked)
    }
  }, [marked])

  const auth = useContext(AuthContext)

  const handleMarked = (id: string) => {
    setMarked([...marked, id])
    // deleteFromFavorites([...marked, id])
  }

  const handleUnMarked = (id: string) => {
    setMarked(marked.filter(i => i !== id))
    // deleteFromFavorites([...marked, id])
  }

  const handleClearMarked = () => {
    deleteFromFavorites(marked)
    setMarked([])
  }

  if (!auth.isAuthenticated) return <Redirect to="/"/>

  return (
    <section>
      <h1>Избранное</h1>
      {!!marked.length && (
        <Button
          onClick={handleClearMarked}
        >
          Удалить отмеченные
        </Button>
      )}
      <div className="fav__container">
        {!!favoritesList.length && favoritesList.map(cat => {
          const isMarkToDelete = marked && marked.includes(cat.id)
          return (
            <div
              className={classNames(
                "fav__item",
                isMarkToDelete && "fav__item--marked"
              )}
              key={cat.id}
            >
              <img src={cat.url} />
              {!isMarkToDelete ? (
              <Button
                onClick={() => handleMarked(cat.id)}
              >
                Удалить
              </Button>
              ) : (
              <Button
                className="btn btn--yellow"
                onClick={() => handleUnMarked(cat.id)}
              >
                Вернуть
              </Button>
              )}
            </div>
          )
        })}
      </div>
    </section>
  )
}

const mapStateToProps = (state: PropsFromState) => ({
  favoritesList: state.favoritesList,
  isAuthUser: state.isAuthUser,
})

export default connect<Props, *, *, *, *, *>(mapStateToProps)(Favorites)

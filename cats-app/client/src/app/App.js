// @flow

import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Route, Switch } from "react-router-dom";
import Header from "~/app/components/Header";
import OfflineNotification from "~/app/components/OfflineNotification";
import Loader from "~/app/components/Loader"
import Main from "~/app/modules/Main";
import Cats from "~/app/modules/Cats";
import Favorites from "~/app/modules/Favorites";
import LoginPage from "~/app/modules/LoginPage";
import RegisterPage from "~/app/modules/RegisterPage";
import {
  updateFavorites,
  updateAuthStatus,
  updateOnlineStatus,
} from "~/app/actions";
import { AuthContext } from "./context/AuthContext"
import { useMakeOnline, useAuth } from "~/app/hooks"

import type { CatItem } from "~/app/types";
import api from "./api";

type Props = {
  updateFavorites: (CatItem[]) => void,
  updateAuthStatus: (boolean) => void,
  updateOnlineStatus: (boolean) => void,
}

export function App ({
  updateFavorites,
  updateAuthStatus,
  updateOnlineStatus,
}: Props) {

  const [loading, setLoading] = useState(false)
  const { login, logout, token, userId, ready } = useAuth()

  const isAuthenticated = !!token
  const online = useMakeOnline()

  useEffect(() => {
    if (isAuthenticated && online && ready) {
      getFavorites()
    }
  }, [isAuthenticated, online, ready])

  useEffect(() => {
    updateAuthStatus(isAuthenticated)
  }, [isAuthenticated])

  useEffect(() => {
    updateOnlineStatus(online)
  }, [online])

  const getFavorites = async () => {
    setLoading(true)
    await api.getFavorites()
      .then(response =>  {
        if (response.success) {
          updateFavorites(response.favoritesList)
          setLoading(false)
        } else throw new Error(response.message)
      })
      .catch(() => {
        setLoading(false)
      })
  }

  const addToFavorites = (cat: CatItem) => {
    if (!isAuthenticated || !online) return
    api.addToFavorites(cat)
      .then(response =>  {
        if (response.success) {
          updateFavorites(response.favoritesList)
          setLoading(false)
        } else throw new Error(response.message)
      })
      .catch(() => {
        setLoading(false)
      })
  }

  const deleteFromFavorites = (catIds: Array<string>) => {
    if (!isAuthenticated || !online) return
    api.deleteFromFavorites(catIds)
      .then(response =>  {
        if (response.success) {
          updateFavorites(response.favoritesList)
          setLoading(false)
        } else throw new Error(response.message)
      })
      .catch((e) => {
        console.log(e.message)
        setLoading(false)
      })
  }

  if (!ready) return <Loader />

  return (
    <AuthContext.Provider value={{
      login, logout, token, userId, isAuthenticated, ready
    }}>
      <Header />
      <Switch>
        <Route exact path="/" component={Main} />
        <Route exact path="/image/" render={() => (
          <Cats
            addToFavorites={addToFavorites}
            deleteFromFavorites={deleteFromFavorites}
          />
        )} />
        <Route exact path="/favorites/" render={() => (
          <Favorites
            addToFavorites={addToFavorites}
            deleteFromFavorites={deleteFromFavorites}
          />
        )} />
        <Route exact path="/login/" component={LoginPage} />
        <Route exact path="/register/" component={RegisterPage} />
      </Switch>
      <OfflineNotification />
    </AuthContext.Provider>
  )

}

const mapDispatchToProps = dispatch => ({
  updateFavorites: favoritesList => dispatch(updateFavorites(favoritesList)),
  updateAuthStatus: status => dispatch(updateAuthStatus(status)),
  updateOnlineStatus: online => dispatch(updateOnlineStatus(online)),
})

export default connect<Props, *, *, *, *, *>(null, mapDispatchToProps)(App)

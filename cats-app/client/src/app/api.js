// @flow

import { performRequest } from "~/app/utils";

export default {
  login (values: Object) {
    return performRequest({
      url: "/api/auth/login",
      method: "POST",
      body: {...values},
    })
  },
  register (values: Object) {
    return performRequest({
      url: "/api/auth/register",
      method: "POST",
      body: {...values},
    })
  },
  getFavorites () {
    return performRequest({
      url: "/api/fav/get",
      method: "GET",
    })
  },
  addToFavorites (values: Object) {
    return performRequest({
      url: "/api/fav/add",
      method: "POST",
      body: {...values},
    })
  },
  deleteFromFavorites (values: Array<string>) {
    console.log("values", values)
    return performRequest({
      url: "/api/fav/delete",
      method: "DELETE",
      body: values,
    })
  },
  getCats (breed?: string) {
    const breedQuery = breed ? `?breed_ids=${breed}` : ""
    const url = `https://api.thecatapi.com/v1/images/search${breedQuery}`
    return performRequest({
      url,
      method: "GET",
    })
  },
  getBreeds () {
    return performRequest({
      url: "https://api.thecatapi.com/v1/breeds/",
      method: "GET",
    })
  }
}

import { combineReducers } from "redux"
import { favorites } from "./favorites"
import { status } from "./status"
import { online } from "./online"

export const rootReducer = combineReducers({
  favoritesList: favorites,
  isAuthUser: status,
  isOnline: online,
})

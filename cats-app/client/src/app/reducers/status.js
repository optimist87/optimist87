// @flow

import { UPDATE_STATUS } from "~/app/actions"

import type {
  StatusAction,
} from "~/app/types";

const initialState = false

export function status (state: boolean = initialState, action: StatusAction) {
  switch(action.type) {
    case UPDATE_STATUS:
      return action.payload
    default:
      return state;
  }
}

// @flow


import { UPDATE_ONLINE } from "~/app/actions"

import type {
  StatusAction,
} from "~/app/types";

const initialState = true

export function online (state: boolean = initialState, action: StatusAction) {
  switch(action.type) {
    case UPDATE_ONLINE:
      return action.payload
    default:
      return state;
  }
}

// @flow

import {
  UPDATE_FAV,
  CLEAR_FAV,
} from "~/app/actions"

import type {
  CatItem,
  FavoritesAction,
} from "~/app/types";

const initialState = []

export function favorites (state: CatItem[] = initialState, action: FavoritesAction) {
  switch(action.type) {
    case UPDATE_FAV:
      return [
        ...action.payload,
      ]
    case CLEAR_FAV:
      return initialState
    default:
      return state
  }
}
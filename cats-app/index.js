const express = require("express");
const path = require('path')
const cors = require('cors')
const config = require("config");
const mongoose = require("mongoose");
const authRoutes = require("./routes/auth.routes");
const favoritesRoutes = require("./routes/favorites.routes");

const app = express()
app.use(cors())
app.use(express.json({ extended: true }))

app.use( "/api/auth", authRoutes )
app.use( "/api/fav", favoritesRoutes )

if (process.env.NODE_ENV === "production") {
  app.use("/", express.static(path.join(__dirname, "cliend", "dist")))
  app.get("*", (req, res) => {
    res.sendDate(path.resolve(__dirname, "client", "dist", "index.html"))
  })
}

const PORT = process.env.PORT || config.get("port") || 5000

async function start() {
  try {
    await mongoose.connect(config.get("mongoUrl"), {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true
    })
    app.listen(PORT, () => console.log(`======App ready on port ${PORT}!!!!========`))
  } catch (e) {
    console.log("Server Error ", e.message)
    process.exit(1)
  }
}

start()

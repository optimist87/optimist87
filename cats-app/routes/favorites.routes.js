const { Router } = require("express")
const jwt = require("jsonwebtoken")
const { check, validationResult } = require('express-validator')
const Favorite = require("../models/Favorite")
const User = require("../models/User")
const auth = require("../middleware/auth.middleware")

const router = Router()

// add favorites
// /api/fav/add
router.post(
  "/add",
  auth,
  [
    check("id", "required").notEmpty(),
    check("url", "required").notEmpty(),
    check("width", "required").notEmpty(),
    check("height", "required").notEmpty(),
  ],
  async (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) return res.status(422).json({ errors: errors.array() })
    const { id, url, width, height } = req.body

    try {
      let favoriteItem = await Favorite.findOne({ id })
      if (favoriteItem) return res.status(422).json({ message: "Уже в избранном" })
      favoriteItem = new Favorite({
        id, url, width, height,
        owner: req.user.userId
      })

      await favoriteItem.save()

      const favoritesList = await Favorite.find({ owner: req.user.userId }).sort({ date: -1 })

      res.status(201).json({ success: true, message: "Добавлено в избранное", favoritesList })

    } catch (e) {
      console.error(e.message)
      return res.status(500).json({
        success: false,
        message: "Что-то пошло не так"
      })
    }
  }
)

// get favorites
// /api/fav/get
router.get(
  "/get",
  auth,
  async (req, res) => {
    try {
      const favoritesList = await Favorite.find({ owner: req.user.userId }).sort({ date: -1 })
      res.json({
        success: true,
        favoritesList
      })
    } catch (e) {
      console.error(e.message)
      res.status(500).json({
        success: false,
        message: "Что-то пошло не так"
      })
    }
  }
)

// delete favorites
// /api/fav/delete
router.delete(
  '/delete',
  auth,
  async (req, res) => {
    console.log(req.body)
    try {
      await Favorite.deleteMany({ owner: req.user.userId, id: { $in: req.body} })
      const favoritesList = await Favorite.find({ owner: req.user.userId }).sort({ date: -1 })
      res.status(200).json({
        success: true,
        message: "Удалено из избранного",
        favoritesList
      })
    } catch (e) {
      console.error(e.message)
      res.status(500).json({
        success: false,
        message: "Что-то пошло не так"
      })
    }
  }
)

module.exports = router

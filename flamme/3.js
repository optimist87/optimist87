const sum = makeSum();

function makeSum() {
  return function(a) {
    var count = a;
    function fu(b) {
      if (b && !isNaN(b)) {count += b}
      return fu;
    }
    fu.toString = function() {
      return count;
    }
    return fu;
  }
}

console.log(
  sum(1)(1)(2)() // 4
)

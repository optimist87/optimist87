let globalCounter = 0;

let counter = counterFunction();
function counterFunction() {
  var count = 0;
  return function() {
    console.log(++count)
  }
}
counter(); // 1
counter(); // 2
counter(); // 3
counter(); // 4
counter(); // 5

// let counter = counterFunction();

// function counterFunction() {
//   console.log(++globalCounter)
// }

// counter()
// counter()
// counter()

//+

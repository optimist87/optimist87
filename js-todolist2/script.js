function ready() {


  // Перезаписывает хранилище на основе текущего списка задач
  function storageUpdate() {
    localStorage.setItem('listStorage', JSON.stringify(listStorage))
  }


  // Восстанавливает список при загрузке страницы
  function storageRestore() {
    if (localStorage.listStorage) listStorage = JSON.parse(localStorage.getItem("listStorage"))
    listTemplateBuild(listStorage)
  }

  // Очищает хранилище
  function storageClear() {
    localStorage.removeItem('listStorage')
  }

  // собирает шаблон и рендерит на страницу
  function listTemplateBuild(e) {
    var listStorage = e
    var renderList = ''
    if (!listStorage) return
    for (var i = 0; i < listStorage.length; i++) {
      for (key in listStorage[i]) {
        listItemTemplate = `
          <div class="list-item">
            <div class="item-title">${key}</div>
            <div class="item-text">${listStorage[i][key]}</div>
            <div class="item-edit" data-key="${i}">Редактировать</div>
            <div class="item-remove" data-key="${i}">Удалить</div>
          </div>
        `
        renderList += listItemTemplate
      }
    }
    document.querySelector('#list-container').innerHTML = renderList;
  }


  // Добавление элемента
  function addItem() {
    var $form = document.querySelector('#totolist').querySelector('.form'),
        $titleInput = $form.querySelector('.item-title'),
        $textArea = $form.querySelector('.item-text'),
        titleInputValue = $titleInput.value,
        textAreaValue = $textArea.value,
        listStorageItem = {}

    if (titleInputValue.length && textAreaValue.length) {
      listStorageItem[titleInputValue] = textAreaValue
      listStorage.push(listStorageItem)
      listTemplateBuild(listStorage)
      $titleInput.value = ''
      $textArea.value = ''
      storageUpdate()
    }
  }

  // Удаление элемента
  function removeItem(e) {
    if (!e.target.classList.contains('item-remove')) return
    var thisKey = e.target.getAttribute('data-key')
    listStorage.splice(thisKey, 1)
    listTemplateBuild(listStorage)
    storageUpdate()
  }

  // Редактирование элемента
  function editItem(e) {
    if (!e.target.classList.contains('item-edit')) return
    var thisKey = e.target.getAttribute('data-key'),
        $form = document.querySelector('#totolist').querySelector('.form'),
        $titleInput = $form.querySelector('.item-title'),
        $textArea = $form.querySelector('.item-text'),
        $saveBtn = $form.querySelector('.item-save')

    for (title in listStorage[thisKey]) {
      var thisTitle = title,
          thisText = listStorage[thisKey][title]
      $titleInput.value = thisTitle
      $textArea.value = thisText
      $form.classList.add('state-edit')
      $saveBtn.setAttribute('data-key', thisKey)
    }
  }

  // Сохранение элемента
  function saveItem(e) {
    var thisKey = e.target.getAttribute('data-key'),
        $form = document.querySelector('#totolist').querySelector('.form'),
        $saveBtn = $form.querySelector('.item-save'),
        $titleInput = $form.querySelector('.item-title'),
        $textArea = $form.querySelector('.item-text'),
        titleInputValue = $titleInput.value,
        textAreaValue = $textArea.value,
        listStorageItem = {}

    if (titleInputValue.length && textAreaValue.length) {
      listStorageItem[titleInputValue] = textAreaValue
      listStorage.splice(thisKey, 1, listStorageItem)
      listTemplateBuild(listStorage)
      $titleInput.value = ''
      $textArea.value = ''
      $saveBtn.removeAttribute('data-key')
      storageUpdate()
    }
  }



  var listStorage = []
  storageRestore()

  document.querySelector('.clear-stor').onclick = storageClear
  document.querySelector('.item-add').onclick = addItem
  document.querySelector('.item-save').onclick = saveItem
  document.addEventListener('click', editItem)
  document.addEventListener('click', removeItem)

}

document.addEventListener("DOMContentLoaded", ready);

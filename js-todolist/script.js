// Добавляет в блок ul списка. Можно все и на div-ах сделать тогда это не понадобится
function todolistInit() {
  var ulList = '<ul class="list"></ul>';
  $('.list-container').append(ulList)
}

function todolistHandler() {
  // Для записывания правильного значения в счетчик при перезагрузке страницы
  // Если список пустой, то записывается 0
  function counterInit() {
    if (!$('.list > li').length) {
      return $('.list > li').length
    } else {
      return $('.list > li:last-child').data('uniq') + 1
    }
  }
  var $todoList = $('#totolist'),
      $form = $('#totolist').find('.form'),
      $btnAdd = $form.find('.item-add'),
      $btnEditSubmit = $form.find('.item-edit-submit'),
      $titleInput = $form.find('.item-title'),
      $textArea = $form.find('.item-text'),
      uniqKey = counterInit()

  // Добавление элемента
  function addItem() {

    var titleInputValue = $titleInput.val()
    var textAreaValue = $textArea.val()

    if (titleInputValue.length && textAreaValue.length) {

      var listItem = '<li class="list-item" data-uniq="' + uniqKey + '"><span class="title-item">' + titleInputValue + '</span><span class="text-item">' + textAreaValue + '</span><span class="remove" data-uniq="' + uniqKey + '">удалить</span><span class="edit" data-uniq="' + uniqKey + '">ред.</span></li>'

      $('.list').append(listItem)
      $titleInput.val('')
      $textArea.val('')
      storageUpdate()
      ++uniqKey
    }
  }

  // Редактирование элемента
  function editItem() {
    var $btnEdit = $(this),
        uniqKey = $btnEdit.data('uniq'),
        $thisListItem = $('.list').find('.list-item[data-uniq="' + uniqKey + '"]'),
        $thisTitle = $thisListItem.find('.title-item')
        thisTitleVal = $thisTitle.text(),
        $thisText = $thisListItem.find('.text-item'),
        thisTextVal = $thisText.text()
    $titleInput.val(thisTitleVal)
    $textArea.val(thisTextVal)
    $form.addClass('state-edit')
    
    $btnEditSubmit.click(editSubmit)
    // TODO Запара
    function editSubmit() {
      var $titleInput = $form.find('.item-title'),
          $textArea = $form.find('.item-text'),
          titleInputValue = $titleInput.val(),
          textAreaValue = $textArea.val()
      $thisTitle.text(titleInputValue)
      $thisText.text(textAreaValue)
      $titleInput.val('')
      $textArea.val('')
      storageUpdate()
      $form.removeClass('state-edit')
    }

  }

  // Удаление элемента
  function removeItem() {
    var uniq = $(this).data('uniq')
    $('.list-item[data-uniq="' + uniq + '"]').remove()
    storageUpdate()
  }

  $btnAdd.click(addItem)
  $todoList.delegate('.edit', 'click', editItem)
  $todoList.delegate('.remove', 'click', removeItem)

}

// Перезаписывает хранилище на основе текущего списка задач
function storageUpdate() {

  if ($('.list')) {
    var listArray = []

    if($('.list').children('li').length) {
      $('.list').children('li').each(function() {
        listArray.push($(this)[0].outerHTML)
      })
      localStorage.setItem('listStorage', JSON.stringify(listArray))
    } else {
      localStorage.removeItem('listStorage')
    }

  }
}

// Восстанавливает список при загрузке страницы
function storageRestore() {
  if (!localStorage.listStorage) return
  var listArray = JSON.parse(localStorage.getItem("listStorage"));
  var count = listArray.length
  for (var i=0; i < count; i++) {
    var listItem = listArray[i]
    $('.list').append(listItem)
  }
}

// Очищает хранилище
function storageClear() {
  $('.clear-stor').click(function(){
    localStorage.removeItem('listStorage')
  })
}

$(document).ready(function(){

  todolistInit()
  storageRestore()
  storageClear()
  todolistHandler()

})

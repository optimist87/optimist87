import React, { Component } from 'react';

class Template extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      editMode: false,
      title: "",
      text: ""
    }
  }

  editItem = () => {
    this.setState({editMode: !this.state.editMode})
    this.props.editItem()
  }

  saveItem = () => {
    let newTitle = this.state.title
    let newTxt = this.state.text
    this.props.saveItem(newTitle, newTxt, this.props.item.id)
    this.setState({editMode: !this.state.editMode})
  }

  removeItem = () => {
    this.props.removeItem(this.props.item.id)
  }

  handleInputChange = (e) => {
    const target = e.target
    const name = target.name
    this.setState({
      [name]: target.value
    })
  }

  renderNorm = () => {
    let title = this.props.item.title;
    let text = this.props.item.text;

    return (
      <div className="list-item">
        <div className="item-title">{title}</div>
        <div className="item-text">{text}</div>
        <div className="btn btn-edit" onClick={this.editItem}>Редактировать</div>
        <div className="btn btn-remove" onClick={this.removeItem}>Удалить</div>
      </div>
    );
  }
  renderEdit = () => {
    let title = this.props.item.title;
    let text = this.props.item.text;

    return (
      <div className="list-item">
        <input type="text" name="title" defaultValue={title} onChange={this.handleInputChange} />
        <input type="text" name="text" defaultValue={text} onChange={this.handleInputChange} />
        <button className="btn btn-save" onClick={this.saveItem}>Сохранить</button>
      </div>
    );
  }

  render() {
    if (this.state.editMode) {
      return this.renderEdit()
    } else {
      return this.renderNorm()
    }
  }

}

export default Template;

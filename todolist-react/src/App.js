import React, { Component } from 'react';
import classNames from 'classnames';
import Template from './Template';



class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      editMode: false,
      itemsData: [],
      title: '',
      text: '',
    }
  }

  componentDidMount() {
    console.log("did mount App");
    let store = JSON.parse(localStorage.getItem("todolistTada"))
    localStorage.getItem("todolistTada") ? this.setState({ itemsData: store }) : null
  }

  clearStore = () => {
    localStorage.removeItem("todolistTada")
  }

  editMode = () => {
    this.setState({editMode: !this.state.editMode})
  }

  updateStore = () => localStorage.setItem("todolistTada", JSON.stringify(this.state.itemsData))

  addItem = () => {
    let title = this.state.title
    let text = this.state.text
    let id = Date.now()
    if (!title && !text) return

    this.setState({
      itemsData: [...this.state.itemsData, {title, text, id}]
    }, this.updateStore);

    this.clearForm()
  }

  saveItem = (title, text, id) => {
    console.log('save App')
    var arrItemsData= this.state.itemsData
    arrItemsData.map((item) => {
      if (item.id === id) {
        item.title = title
        item.text = text
      }
    })
    this.setState({
      itemsData: [...arrItemsData]
    }, this.updateStore)
    this.setState({editMode: !this.state.editMode})
  }

  editItem = () => {
    console.log('edit')
    this.setState({
      editMode: !this.state.editMode
    })
  }

  removeItem = (id) => {
    console.log('remove')
    var arrItemsData= this.state.itemsData
    var newArrItemsData = arrItemsData.filter((item) => {
      return item.id !== id
    })
    this.setState({
      itemsData: [...newArrItemsData]
    }, this.updateStore)
  }

  handleInputChange = (e) => {
    const target = e.target
    const name = target.name
    this.setState({
      [name]: target.value
    })
  }

  clearForm = () => {
    this.setState({
      title: '',
      text: '',
    })
  }

  render() {
    let editMode = this.state.editMode
    const { title } = this.state
    const { text } = this.state

    return (
      <div className="todolist-container">
        <h1>Список задач</h1>
        <div className="totolist">
          <div id="list-container">
            {this.state.itemsData.map((item) => {
              return <Template
              key={item.id}
              item={item}
              editItem={this.editItem}
              removeItem={this.removeItem}
              saveItem={this.saveItem}
              />
            }
            )}
          </div>
          <hr />
          <div className={classNames('form', { 'state-edit': editMode })}>
            <input className="form-title" type="text" name="title" value={title} placeholder="Название задания" onChange={this.handleInputChange} />
            <textarea className="form-text" name="text" value={text} placeholder="Содержание" onChange={this.handleInputChange} />
            <button className="btn btn-add" onClick={this.addItem} disabled={'true' && editMode}>Добавить</button>
          </div>
        </div>
        <button className="btn clear-stor" onClick={this.clearStore}>Сброс</button>
      </div>
    );
  }
}

export default App;
